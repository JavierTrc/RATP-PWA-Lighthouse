'use strict';

const Audit = require('lighthouse').Audit;

const MAX_API_TIME = 3000;

class ApiLoadAudit extends Audit {
    static get meta() {
        return {
            id: "api-audit",
            category: 'MyPerformance',
            title: 'API Audit',
            description: 'Request from the RATP API received',
            failureDescription: 'RATP API request takes too long',
            helpText: 'Used to measure time from navigationStart to when the api' +
            ' response is received.',

            requiredArtifacts: ['TimeToApi']
        };
    }

    static audit(artifacts) {
        const loadedTime = artifacts.TimeToApi;

        const belowThreshold = loadedTime <= MAX_API_TIME;

        return {
            rawValue: loadedTime,
            displayValue: `${loadedTime.toFixed(2)} ms`,
            score: belowThreshold ? 1:0
        };
    }
}

module.exports = ApiLoadAudit;